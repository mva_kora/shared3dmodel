# README #

### What is this repository for? ###

This is the repository for the project titled "Joint 3D model viewing" in the lecture "Distributed Multimedia Applications" (summer term 2016).

### How do I get set up? ###

1. Visual Studio 2015 Enterprise Update 3
2. Node.js v6.3.1 https://nodejs.org/en/ (Current/latest features)
3. To install the relevant npm packages, execute "npm install" in the directory that contains the "package.json" file.
4. Start the Visual Studio Debugger
5. Open your favourite browser and navigate to http://localhost:3000
6. Connect clients to the server @http://serverURL:3000

### Contact ###
* Dennis Kölbel: dennis.koelbel@hs-osnabrueck.de
* Christoph Rahn: christoph.rahn@hs-osnabrueck.de