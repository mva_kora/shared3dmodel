/*
 * GET home page.
 */

exports.index = function (req, res) {
    res.render('index', { title: 'WebGL-Joint 3D-Model Viewer', year: new Date().getFullYear() });
};

exports.model = function (req, res) {
    res.render('model', { title: 'Model-View', year: new Date().getFullYear() });
};

exports.help = function (req, res) {
    res.render('help', { title: 'Help', year: new Date().getFullYear() });
};


