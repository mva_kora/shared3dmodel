
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var http = require('http');
var path = require('path');
var WebSocketServer = require('ws').Server;

var app = express();

// Converts from degrees to radians.
Math.radians = function (degrees) {
    return degrees * Math.PI / 180;
};

// Converts from radians to degrees.
Math.degrees = function (radians) {
    return radians * 180 / Math.PI;
};

//Websocket Server=============================
var clients = [ ];			//List of connected clients
var listOfDates = [];
var blockingClient;
var mouseIsBlocked = false;
var worldState = [0,0,3, Math.radians(45), Math.radians(45), Math.radians(0), 0.50, 0.80];

var webSocketServer = new WebSocketServer({ port: 3001 });
console.log("Websocket-Server listening on Port: 3001");

webSocketServer.on('connection', function(webSocket) {
    webSocket.on('message', function(message) {	
    var messageTokens = message.split("?");
        
    //Message "connect" received
    if ( messageTokens[0] == 'connect' )
	{
		clients.push(webSocket);
		listOfDates.push(new Date());		
        console.log("Client " + webSocket.upgradeReq.connection.remoteAddress + " connected. (" + new Date() + ")");
        webSocket.send('data?' + JSON.stringify(worldState));	
    }
    //Message "data" received
    else if (messageTokens[0] == 'data' && (blockingClient == webSocket)) {        
        console.log('[' + webSocket._socket.remoteAddress + '] data send');
        worldState = JSON.parse(messageTokens[1]).slice();
        for (var i = 0; i < clients.length; i++) {
            if (clients[i] != webSocket) {
                    clients[i].send(message);
            }
        }
    }
    //Message "activeMouse" received
    else if (messageTokens[0] == 'activeMouse' && !mouseIsBlocked) {
        console.log('[' + webSocket._socket.remoteAddress + '] MouseButtons blocked' );
        mouseIsBlocked = true;
        blockingClient = webSocket;
        for (var i = 0; i < clients.length; i++) {
            if (clients[i] != webSocket) {
                clients[i].send(message);
            }
        }
    }
    //Message "inactiveMouse" received
    else if (messageTokens[0] == 'inactiveMouse' && mouseIsBlocked) {
            console.log('[' + webSocket._socket.remoteAddress + '] MouseButtons unblocked');
        mouseIsBlocked = false;
        for (var i = 0; i < clients.length; i++) {
            if (clients[i] != webSocket) {
                clients[i].send(message);
            }
        }
    }
    else {
            console.log('[' + webSocket._socket.remoteAddress + '] else entered');
        //webSocket.close(1000, 'Wrong message during connection');	//Close on wrong message
    }
    });
    webSocket.on('close', function close() {
		for (var i = 0; i < clients.length; i++)
		{
			if( clients[i] == webSocket )
			{
				clients.splice(i, 1);		// delete client from list
				listOfDates.splice(i, 1);
				console.log("Client "+webSocket.upgradeReq.connection.remoteAddress+" disconnected. ("+new Date()+")");
			}
		}
	});
});

// all environments
app.set('port', 3000);
//app.set('viewebSocket', path.join(__dirname, 'viewebSocket'));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/help', routes.help);
app.get('/model', routes.model);

http.createServer(app).listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});
