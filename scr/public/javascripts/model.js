﻿//#region WebSocket Connection

function showMessage(text) {
    document.getElementById('message').innerHTML = text;
}

// WebSocket creation
console.log("Connect activated");
var url = window.location;
var port = parseInt(url.port) + 1;
var wsServer = 'ws://' + url.hostname + ':' + port;

var hasConnection = false;      //Connection active
var mouseBlocked = false;       //Mouse blocked locally or from another client
var activeMouseWheel = false;   //MouseWheel in action
var hasPendingChange = false;   //Changes that have to be send

var socket = new WebSocket(wsServer);

socket.onopen = function () {
    hasConnection = true;
    console.log('Client connected!')
    socket.send('connect');
    //Message in some situations not shown in edge,
    //because function is called before element "message" is created
    setTimeout(function () {                                
        if (document.getElementById('message')) {
            showMessage('Connected to WebSocket-Server.');
        }
    }, 100);
    
};

socket.onmessage = function (message)  {
    var messageTokens = message.data.split("?");
    
    //message "activeMouse" received, another client has pressed a Mousebutton
    if (messageTokens[0] == 'activeMouse') {
        console.log('mouseBlocked = true');
        mouseBlocked = true;
    }
    //message "inactiveMouse" received, another client has released a Mousebutton
    else if (messageTokens[0] == 'inactiveMouse') {        
        console.log('mouseBlocked = false');
        mouseBlocked = false;
    }
    else if (messageTokens[0] == 'data') {
        console.log('Data received');
        var worldState = JSON.parse(messageTokens[1]);        
        camera.position.set(worldState[0], worldState[1], worldState[2]);
        cube.rotation.set(worldState[3], worldState[4], worldState[5]);
        ambientLight.intensity = worldState[6];
        directionalLight.intensity = worldState[7];
        console.log("sendData: " + messageTokens[1]);
    }
    else {
        if (document.getElementById('message')) {
            showMessage(messageTokens[0]);
        }
    }
};

socket.onerror = function (message) {
    if (document.getElementById('message')) {
        showMessage('Sorry but there was an error.');
        hasConnection = false;
    }
};

socket.onclose = function () {
    if (document.getElementById('message')) {
        showMessage('Server offline.');
        hasConnection = false;
    }
};

//#endregion WebSocket Connection

// Converts from degrees to radians.
Math.radians = function (degrees) {
    return degrees * Math.PI / 180;
};

// Converts from radians to degrees.
Math.degrees = function (radians) {
    return radians * 180 / Math.PI;
};

//#region Three.js
var camera, scene, renderer, cube, stats;
var ambientLight, directionalLight;

var canvas;

var mouseX = 0, mouseY = 0;

var rotationsMatrixX = new THREE.Matrix4(),
    rotationsMatrixY = new THREE.Matrix4(),   
    rotationsMatrixZ = new THREE.Matrix4(),
    rotationsMatrix = new THREE.Matrix4(),
    translationsMatrix = new THREE.Matrix4(),
    transformationMatrix = new THREE.Matrix4();

var cameraPosition, cubeRotation;

window.innerHeight = window.innerHeight * 0.8;

var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;
var sceneStatus;

//Three.js
init();
animate();

function init() {
    
    //canvas
    canvas = document.createElement('div');
    canvas.className = 'myCanvas';
    
    document.body.appendChild(canvas);
    canvas.oncontextmenu = function () {
        return false;
    }

    //camera
    camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
    camera.position.z = 3;
    
    // scene
    scene = new THREE.Scene();
    
    //ambientlights
    ambientLight = new THREE.AmbientLight( 0xffffff, 0.50 );
    scene.add(ambientLight);
  
    //directlights
    directionalLight = new THREE.DirectionalLight(0xffffff, 0.80);
    directionalLight.position.set(0, Math.radians(45), 0);
    scene.add(directionalLight);
    
    //object
    console.log("uploadModelInput: " + sessionStorage.getItem('uploadModelName'));
    if (sessionStorage.getItem('uploadModelName') == '') {
        var geometry = new THREE.BoxGeometry( 1, 1, 1 );
        var material = new THREE.MeshPhongMaterial({ color: 0x1C86EE });
        cube = new THREE.Mesh(geometry, material);
        scene.add(cube);
        cube.rotation.set(Math.radians(45), Math.radians(45), Math.radians(0));
    }
    else {
        //Wrong Filetype
        alert("Not yet implemented!")
        var geometry = new THREE.BoxGeometry( 1, 1, 1 );
        var material = new THREE.MeshPhongMaterial({ color: 0x1C86EE });
        cube = new THREE.Mesh(geometry, material);
        scene.add(cube);
        cube.rotation.set(Math.radians(45), Math.radians(45), Math.radians(0));
    }
    
    //renderer
    renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    canvas.appendChild(renderer.domElement);

    //rotate object
    document.addEventListener('mousedown', onDocumentMouseDown, false);

    //zoom camera
    document.addEventListener('mousewheel', onDocumentMouseWheel, false);
    
    //resizeWindow
    window.addEventListener('resize', function () {
        renderer.setSize(window.innerWidth, window.innerHeight);
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
    });
    
    stats = new Stats();
    stats.domElement.style.position = 'absolute';
    stats.domElement.style.top = '120px';
    //stats.domElement.style.right = '0px';
    document.body.appendChild(stats.domElement);
}

function onResetButtonClicked() {

    if (!mouseBlocked) {
        console.log("onResetButtonClicked activated");
        
        //reset camera, cube and lights
        cube.rotation.set(Math.radians(45), Math.radians(45), Math.radians(0));
        camera.position.set(0, 0, 3);
        ambientLight.intensity = 0.50;
        directionalLight.intensity = 0.80;

        //MouseButton active, Mousebutton of other users disabled
        //socket.send('activeMouse');
        //mouseBlocked = true;
        hasPendingChange = true;
    }
    //mouseBlocked = false;
}

//ambientLightButton
function onAmbientLightButtonClicked() {

    if (!mouseBlocked) {

        console.log("onAmbientLightButtonClicked activated");
        
        if (ambientLight.intensity == 0.0)
            ambientLight.intensity = 0.50;
        else
            ambientLight.intensity = 0.0;

        //MouseButton active, Mousebutton of other users disabled
        //socket.send('activeMouse');
        //mouseBlocked = true;
        hasPendingChange = true;
    }
    //mouseBlocked = false;

}

//directionalLightButton
function onDirectionalLightButtonClicked() {
    
    if (!mouseBlocked) {
        console.log("onDirectionalLightButtonClicked activated");
        
        if (directionalLight.intensity == 0.0)
            directionalLight.intensity = 0.80;
        else
            directionalLight.intensity = 0.0;

        //MouseButton active, Mousebutton of other users disabled
        //socket.send('activeMouse');
        //mouseBlocked = true;
        hasPendingChange = true;
    }
    //mouseBlocked = false;
    
}


function onDocumentMouseDown(event) {
    console.log("onDocumentMouseDown activated");
    event.preventDefault();
    
    if (!mouseBlocked) {
        //MouseButton active, Mousebutton of other users disabled


        socket.send('activeMouse');
        mouseBlocked = true;
        
        mouseX = event.clientX;
        mouseY = event.clientY;
        
        switch (event.which) {
            case 1:
                console.log("Left button is pressed");
                document.addEventListener('mousemove', onDocumentMouseMoveButton1, false);
                break;
            case 2:
                console.log("Middle button is pressed");
                document.addEventListener('mousemove', onDocumentMouseMoveButton2, false);
                break;
            case 3:
                console.log("Right button is pressed");
                document.addEventListener('mousemove', onDocumentMouseMoveButton3, false);
                break;
        }
        
        transformationMatrix = rotationsMatrix.multiplyMatrices(rotationsMatrix, translationsMatrix);
        
        document.addEventListener('mouseup', onDocumentMouseUp, false);
        document.addEventListener('mouseout', onDocumentMouseOut, false);
    }
    
}

function onDocumentMouseMoveButton1(event) {

    console.log("onDocumentMouseMoveButton1 activated");

    var deltaX = event.clientX - mouseX;
    var deltaY = event.clientY - mouseY;

	mouseX = event.clientX;
    mouseY = event.clientY;
    
    // deltaX and deltaY swapped for cube rotation to accomodate for 2D->3D axis changes
    rotationsMatrixX.makeRotationX(deltaY / 100);
    rotationsMatrixY.makeRotationY(deltaX / 100);
    
    rotationsMatrix = rotationsMatrixX.multiply(rotationsMatrixY);
    
    cube.applyMatrix(rotationsMatrix);
    
    hasPendingChange = true;
}

function onDocumentMouseMoveButton2(event) {
    
    console.log("onDocumentMouseMoveButton2 activated");
    
    var deltaX = event.clientX - mouseX;
    
    mouseX = event.clientX;
    
    // deltaX and deltaY swapped for cube rotation to accomodate for 2D->3D axis changes
    rotationsMatrixZ.makeRotationZ(deltaX / 100);
    
    cube.applyMatrix(rotationsMatrixZ);

    hasPendingChange = true;
}

function onDocumentMouseMoveButton3(event) {
    
    console.log("onDocumentMouseMoveButton3 activated");
    event.preventDefault();
    
    var deltaX = event.clientX - mouseX;
    var deltaY = event.clientY - mouseY;
    
    mouseX = event.clientX;
    mouseY = event.clientY;

    translationsMatrix.makeTranslation(-(deltaX / 100), (deltaY / 100), 0);

    camera.applyMatrix(translationsMatrix);

    hasPendingChange = true;
}

function onDocumentMouseUp(event) {
    
    console.log("onDocumentMouseUp activated");
    socket.send('inactiveMouse');
    mouseBlocked = false;
    document.removeEventListener('mousemove', onDocumentMouseMoveButton1, false);
    document.removeEventListener('mousemove', onDocumentMouseMoveButton2, false);
    document.removeEventListener('mousemove', onDocumentMouseMoveButton3, false);
    document.removeEventListener('mouseup', onDocumentMouseUp, false);
    document.removeEventListener('mouseout', onDocumentMouseOut, false);

}

function onDocumentMouseOut(event) {
    console.log("onDocumentMouseOut activated");
    socket.send('inactiveMouse');
    mouseBlocked = false;
    document.removeEventListener('mousemove', onDocumentMouseMoveButton1, false);
    document.removeEventListener('mousemove', onDocumentMouseMoveButton2, false);
    document.removeEventListener('mousemove', onDocumentMouseMoveButton3, false);
    document.removeEventListener('mouseup', onDocumentMouseUp, false);
    document.removeEventListener('mouseout', onDocumentMouseOut, false);

}

function onDocumentMouseWheel(event) {
    console.log("onDocumentMouseWheel activated");
    event.preventDefault();

    if (!mouseBlocked && !activeMouseWheel) {
        socket.send('activeMouse');
        mouseBlocked = true;
        camera.position.z += (-1) * 0.005 * event.wheelDeltaY;
        hasPendingChange = true;
        activeMouseWheel = true;
    }
    
}

function animate() {

    if (activeMouseWheel) {
        socket.send('inactiveMouse');
        mouseBlocked = false;
        activeMouseWheel = false;
    }

    requestAnimationFrame(animate);
    
    //show rotation an d positin on website
    document.getElementById('td-1-1').innerHTML = Math.degrees(cube.rotation.x).toFixed(2);
    document.getElementById('td-1-2').innerHTML = Math.degrees(cube.rotation.y).toFixed(2);
    document.getElementById('td-1-3').innerHTML = Math.degrees(cube.rotation.z).toFixed(2);
    document.getElementById('td-2-1').innerHTML = camera.position.x.toFixed(2);
    document.getElementById('td-2-2').innerHTML = camera.position.y.toFixed(2);
    document.getElementById('td-2-3').innerHTML = camera.position.z.toFixed(2);
    
    //send Rotation and Position data to Server
    sceneStatus = [camera.position.x, camera.position.y, camera.position.z, cube.rotation.x, cube.rotation.y, cube.rotation.z, ambientLight.intensity, directionalLight.intensity];
    var message = 'data?' + JSON.stringify(sceneStatus);
    if (hasConnection && hasPendingChange) {
        console.log("updated data send");
        socket.send(message);
    }       
  
    render();
    stats.update();
    hasPendingChange = false;
}

function render() {

    renderer.render(scene, camera);

}

//#endregion Three.js